import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class TesteEscreve {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testUnidade() {
		assertEquals ("seis",escreve.escritor(6));
	}

	@Test
	public void testDezena() {
		assertEquals ("doze",escreve.escritor(12));
	}
	
	@Test
	public void testCentena() {
		assertEquals ("duzentos e doze",escreve.escritor(212));
	}
	@Test
	public void testMilhar() {
		assertEquals ("mil",escreve.escritor(1000));
	}
}
